# Tensorflow

Built `Tensorflow` with:
- Python 3.6
- Cuda 9.2
- cuDNN 7.1.4
- NCCL 2.2.13

2 versions:
- [Tensorflow 1.10](./tensorflow-1.10.1-cp36-cp36m-linux_x86_64.whl)
- [Tensorflow 1.9rc](./tensorflow-1.9.0rc0-cp36-cp36m-linux_x86_64.whl)